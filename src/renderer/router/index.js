import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'splash-page',
      component: require('@/components/SplashPage').default
    },
    {
      path: '/info',
      name: 'info-page',
      component: require('@/components/InfoPage').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
