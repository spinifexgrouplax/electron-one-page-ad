# electron-ad

> A simple splash/static Windows app built on Electron/Vue/Webpack.

#### Build Setup

``` bash
# install dependencies
npm install

# start dev mode
npm start

# build electron application for production
npm run build:win32

# lint all JS/Vue component files in `src/`
npm run lint

```

---

#### Requirements

* Environment file (.env) at root directory, containing:

``` bash
# Leave this blank when app is on production
IS_DEV=TRUE

# How long (in seconds) before app resets back to splash page
TIMEOUT=30

# If we only need a static photo viewer, set this to blank or FALSE
SPLASH_ENABLED=TRUE

# The file names (path not included) of the assets to use
SPLASH_FILE=splash.mp4
BACKGROUND_FILE=background.jpg
```

* Assets: `background.jpg` and `splash.mp4` must be placed in `resources/app/dist/electron/static`.

#### Starting the app

* Use START_APP.bat to securely run the app.

#### Exiting the app

Double-tap on the upper-left corner twice, within 5 seconds.

This project was generated with [electron-vue](https://github.com/SimulatedGREG/electron-vue)@[7c4e3e9](https://github.com/SimulatedGREG/electron-vue/tree/7c4e3e90a772bd4c27d2dd4790f61f09bae0fcef) using [vue-cli](https://github.com/vuejs/vue-cli). Documentation about the original structure can be found [here](https://simulatedgreg.gitbooks.io/electron-vue/content/index.html).
